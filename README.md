# Desafio_website

Testes de  automação , usando o framework BDD Cucumber para fornecer documentação para testes automatizados.

## Dependências
* Ruby 

### Recomendações

* Instale o bundler

            $ gem install bundler

* Instalação do Projeto

            $ git@gitlab.com:CintiSteele/desafio_website.git

* Instalar dependencias    
   
            $ bundle install

#### Execute os cenários

            $ cucumber -t @login_sucesso
            
            $ cucumber -t @cadastrar_usuario
