#language: pt

@login_sucesso
Funcionalidade: Login na Inmetrics

Cenario: Realizar login
    Dado que o usuário possa acessar o site da inmetrics
    E preenche o campo usuário
    E preenche o campo senha
    Quando o usuário clica no botão entrar
    Então o usuário é redirecionado para a área logada