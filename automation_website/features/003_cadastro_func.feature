#language: pt

@cadastro_func 
Funcionalidade: Cadastrar Funcionário

#Contexto:  Login
    #Dado que o usuário possa acessar o site da inmetrics
    #E preenche o campo usuário
    #E preenche o campo senha
    #Quando o usuário clica no botão entrar
    #Então o usuário é redirecionado para a área logada

Cenario: Cadastrar Funcionário
    Dado que o usuário possa acessar o site da inmetrics
    E preenche o campo usuário
    E preenche o campo senha
    E o usuário clica no botão entrar
    E aciona o botão novo funcionário
    E preenche o campo nome
    E preenche o campo cpf
    E preenche o campo sexo
    E preenche o campo admissão
    E preenche o campo salário
    E preenche o campo tipo de contração
    Quando o usuário clica no botão submit query
    Então o sistema apresenta a mensagem de usuario cadastrado com sucesso