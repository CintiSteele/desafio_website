#language: pt

@cadastrar_usuario @QA
Funcionalidade: Cadastro de Usuários

Contexto:  acessar sistema
Dado que o usuário possa acessar o site da inmetrics

Cenario: Realizar Cadastro
    Dado que o usuário acesse a página de cadastro
    E informa o campo usuário
    E informa o campo senha
    E informa o campo confirme a senha
    Quando o usuário clica no botão cadastrar
    Então o usuário é redirecionado para a página inicial do cadastro