  Dado('que o usuário acesse a página de cadastro') do
    click_on(class:'txt2 bo1')
  end
  
  Dado('informa o campo usuário') do
    @cadastrar_page = Cadastrar.new
    @cadastrar_page.cadastrar_usuario
  end
  
  Dado('informa o campo senha') do
    @cadastrar_page.cadastrar_senha
  end
  
  Dado('informa o campo confirme a senha') do
    @cadastrar_page.cadastrar_confirmar_senha
  end
  
  Quando('o usuário clica no botão cadastrar') do
    @cadastrar_page.btn_cadastrar
  end
  
  Então('o usuário é redirecionado para a página inicial do cadastro') do
    expect(page).to have_no_content('nCadastre-se')
  end
