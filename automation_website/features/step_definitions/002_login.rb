Dado('que o usuário possa acessar o site da inmetrics') do
    @login_page = Login.new
    @login_page.load
  end
  
  Dado('preenche o campo usuário') do
    @login_page.informar_usuario
  end
  
  Dado('preenche o campo senha') do
    @login_page.informar_senha
  end
  
  Quando('o usuário clica no botão entrar') do
    @login_page.btn_entrar
  end
  
  Então('o usuário é redirecionado para a área logada') do
    expect(page).to have_no_content('nCadastre-se')
  end