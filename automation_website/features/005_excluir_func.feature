#language: pt

@excluir_funcionario
Funcionalidade: Excluir Funcionário

Cenario: Excluir Usuário
    Dado que o usuário logue no site da inmetrics
    E informe o nome do funcionario no campo pesquisar
    Quando o usuário clica no botão excluir
    Então o sistema apresenta a mensagem de funcionário atualizações com sucesso