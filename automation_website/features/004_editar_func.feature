#language: pt

@editar_func
Funcionalidade: Editar Funcionário

Cenario: Editar Funcionário
    Dado que o usuário logue no site da inmetrics
    E informe o nome do funcionario no campo pesquisar
    E clica no botão editar
    E altere os campos apresentados
    Quando o usuário clica no botão submit query
    Então o sistema apresenta a mensagem de informações atualizações com sucesso