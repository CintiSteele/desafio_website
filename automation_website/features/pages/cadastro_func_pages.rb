class CadastrarFunc < SitePrism::Page
    
    element :btn_cadastrar_func, 'a[href="/empregados/new_empregado"]'
    element :input_nome_func, '#inputNome'

    def clicar_cadastrar_func()
        btn_cadastrar_func.click
  end

    def preencher_nome_func()
        input_nome_func.set("AutomationTQA")
    end

    def cadastrar_senha()
        input_senha.set("automacao")
        #find('[name="pass"]').set("automacao")
    end

    def cadastrar_confirmar_senha()
        input_confirmar_senha.set("automacao")
        #find('[name="confirmpass"]').set("automacao")
    end

    def clicar_botao_cadastrar()
        btn_cadastrar.click
        #find('.login100-form-btn').click
    end

end
