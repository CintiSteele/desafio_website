class Cadastrar < SitePrism::Page
    
    element :clicar_link_cadastrar, "a.txt2 bo1"
    element :input_usuario, "input[name='username']"
    element :input_senha, "input[name='pass']"
    element :input_confirmar_senha, "input[name='confirmpass']"
    element :btn_cadastrar, ".login100-form-btn"


    def cadastrar_usuario()
        input_usuario.set("AutomationTQA")
    end

    def cadastrar_senha()
        input_senha.set("automacao")
    end

    def cadastrar_confirmar_senha()
        input_confirmar_senha.set("automacao")
    end

    def clicar_botao_cadastrar()
        btn_cadastrar.click
    end

end
