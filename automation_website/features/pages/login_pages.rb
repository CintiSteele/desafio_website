class Login < SitePrism::Page
    set_url 'https://inm-test-app.herokuapp.com/accounts/login/'
    
    element :input_usuario, "input[name='username']"
    element :input_senha, "input[name='pass']"
    element :btn_entrar, ".login100-form-btn"

    
    def informar_usuario()
        input_usuario.set("inmetrics")
    end

    def informar_senha()
        input_senha.set("automacao")
    end

    def clicar_botao_entrar()
        btn_entrar.click
    end

end
